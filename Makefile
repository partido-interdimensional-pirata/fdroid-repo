.DEFAULT_GOAL := all
signal_update_channel := https://updates.signal.org/android/latest.json
# La versión de Signal, se puede especificar por argumento
signal_auto_version := $(shell wget -qO - "$(signal_update_channel)" | jq -r .versionName)
signal_version ?= $(signal_auto_version)
# El destino de la apk
signal_apk := repo/signal-$(signal_version).apk

wire_update := https://wire.com/en/download/
# Obtener la URL de descarga de Wire
wire_url := $(shell wget -qO - $(wire_update) | xmllint --html --xpath "string(//a[contains(@href, '.apk')]/@href)" - 2>/dev/null)
wire_apk := repo/$(notdir $(wire_url))

# Si este archivo existe, correr fdroid
update := tmp/update

fdroid := PATH=/opt/android-sdk/build-tools/29.0.2/:$$PATH fdroid

ANDROID_HOME := /opt/android-sdk
# Exportar todas las variables
export


$(wire_apk):
	wget -O $@ $(wire_url)
	touch $(update)

# Descarga la APK de Signal
$(signal_apk):
	wget -qO - "$(signal_update_channel)" \
		| jq -r .url \
		| wget -i - -O $@
	touch $(update)

# Obtiene el SHA256 del certificado
tmp/cert-$(signal_version):
	keytool -printcert -jarfile $(signal_apk) \
	| grep SHA256 \
	| cut -d " " -f 3 \
	| tr -d ":" \
	| tr -d "\n" \
	> $@

# Obtiene la página con el SHA256
tmp/fingerprint-$(signal_version):
	wget https://signal.org/android/apk/ -qO - \
	| gunzip \
	> $@

# Obtiene el SHA256 de la página
tmp/validate-$(signal_version): tmp/fingerprint-$(signal_version)
	xmllint --html --xpath "string(//span[@class='fingerprint'])" $< 2>/dev/null \
	| tr -d " " \
	| tr -d ":" \
	| tr -d "\n" \
	> $@

# Genera la configuración uniendo las partes públicas con las privadas
config.py: config.base.py config.private.py
	cat $^ > $@
	chmod 600 $@

index:
	if test -f $(update) ; then $(fdroid) update -c; fi

wire-download: $(wire_apk) ## Descargar Wire
wire: wire-download ## Wire
signal-download: $(signal_apk) ## Descargar Signal
signal-verify: tmp/cert-$(signal_version) tmp/validate-$(signal_version) ## Verificar Signal
	diff $^

signal: signal-download ## Descargar y verificar Signal

clean: ## Eliminar archivos temporales
	rm -f tmp/cert-$(signal_version) tmp/validate-$(signal_version) tmp/fingerprint-$(signal_version)
	rm -f config.py

update: config.py ## Actualiza el repositorio
	if test -f $(update); then $(fdroid) update; fi

push: ## Envía los cambios al repositorio remoto
	if test -f tmp/update ; then $(fdroid) deploy -v && rm $(update); fi

fix-permissions: ## Arregla los permisos del repositorio remoto
	ssh root@partidopirata.com.ar chown -R root:http /srv/http/fdroid.partidopirata.com.ar
	ssh root@partidopirata.com.ar chmod -R 750 /srv/http/fdroid.partidopirata.com.ar

all: wire signal update push fix-permissions ## TODO
